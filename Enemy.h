//
// Created by gracz on 2022-06-05.
//

#ifndef PROJEKT_KONCOWY_ENEMY_H
#define PROJEKT_KONCOWY_ENEMY_H

#include "Player.h"
class Enemy: public Player {

    int AD;
public:
    int level;
    int health;
    explicit Enemy(const string & name);
    int attack();
    int getAD();

};


#endif //PROJEKT_KONCOWY_ENEMY_H
