//
// Created by gracz on 2022-06-18.
//

#ifndef PROJEKT_KONCOWY_SFMLMANAGER_H
#define PROJEKT_KONCOWY_SFMLMANAGER_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics.hpp>
#include "HPlayer.h"
#include "Enemy.h"
#include "Player.h"
#include <string>


enum stanGry{TRWA, PRZEGRANA,WYGRANA};
class sfmlManager {

    sf::Texture bohater,barbarian,miecz,tarcza,menu1,miecze,ADlevel,HPlevel;
    sf::Font czcionka;
    sf::Text HPbohater,HPEnemy,ADBohater,ADEnemy,nazwa1,nazwa2,leveltxt,ADleveltxt,HPleveltxt,atak,obrona,czekanie;
    sf::Event event;
    stanGry state;
HPlayer &gracz;
Enemy &wrog;
public:
    sfmlManager(HPlayer &p1, Enemy &p2);
    void play(sf::RenderWindow &win);
    void levelUp(sf::RenderWindow &win);
    void menu(sf::RenderWindow &win);
    bool winner();
    void getWinnerName();
};


#endif //PROJEKT_KONCOWY_SFMLMANAGER_H
