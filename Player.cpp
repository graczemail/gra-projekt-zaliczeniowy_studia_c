//
// Created by c on 5/27/22.
//

#include "Player.h"
#include "string"

using namespace std;

Player::Player(const string &n, int hp, int lvl, int ADamage) : name(n), health(hp), level(lvl), AD(ADamage)
{

}

int Player::getLevel() {
    return level;
}

int Player::getHealth() {
    return health;
}

int Player::getAD() {
    return AD;
}

string Player::getName() {
    return name;
}

bool Player::isAlive() {
    if (health>0){
        return true;
    }
    else{
    return false;
    }

}

void Player::hit(int damage) {
health -= damage;
}

void Player::defend(int defend) {
health+=defend;
}

void Player::wait(int add) {
AD+=add;
}
