//
// Created by gracz on 2022-06-05.
//

#include "Manager.h"

Manager::Manager(HPlayer &p1, Enemy &p2): player1(p1), player2(p2) {
state=RUNNING;
}

void Manager::fight() {
    int level=1;
    while (state == RUNNING and level<6) {

        int ruch,poziomHP;
        int critId=0;
        poziomHP=player1.getHealth();
        while (player1.isAlive() and player2.isAlive()) {
            int dmg1 = player1.getAD();
            int dmg2 = level*5;
            cout<<"Poziom: "<<level<<endl;
            cout << "Zycie gracza: " << player1.getHealth() << endl;
            cout << "Zycie wroga: " << player2.getHealth() << endl;
            menu();

            cin >> ruch;
            switch (ruch) {
                case 1:
                    player2.hit(dmg1);
                    cout << player1.getName() << " atakuje za " << dmg1 << endl;
                    if(critId==1){
                    player1.wait(-10);
                        critId--;
                    };
                    break;
                case 2:
                    player1.defend(5);
                    cout << player1.getName() << " broni sie" << endl;
                    break;
                case 3:
                    player1.wait(15);
                    cout << player1.getName() << " czeka." << endl;
                    cout << "Punkty ataku: " << player1.getAD() << endl;
                    critId++;
                    break;

            }

            cout << player2.getName() << " atakuje za " << dmg2 << endl;

            player1.hit(dmg2);

        }
        getWinnerName();
        if(winner()){
            int reszta= poziomHP-player1.getHealth();
            player1.defend(reszta);
            levelUp();
            level++;

            player2.defend(100+level*5);

        }
        if(!winner()){
            state=LOST;
        }
    }
}
void Manager::levelUp() {
    int punkty = 3;
    int s;
    
    while (punkty > 0) {
        cout<<"------------------------------------"<<endl;
        cout<<"Wybierz 1 zeby dodac 5 pkt ataku"<<endl;
        cout<<"Wybierz 2 zeby dodac 5 pkt zycia"<<endl;
        cout<<"------------------------------------"<<endl;
        cin>>s;
        switch (s) {
            case 1 :
                player1.wait(5) ;
                punkty--;
                break;
            case 2:
                player1.defend(5);
                punkty--;
                break;
        }

    }
}
void Manager::menu() {
    cout<<"------------------------------------"<<endl;
    cout<<"Wybierz 1 zeby zaatakowac "<<endl;
    cout<<"Wybierz 2 zeby bronic(+5 do obrony)"<<endl;
    cout<<"Wybierz 3 zeby  poczekac(+10 do ataku)"<<endl;
    cout<<"------------------------------------"<<endl;
}

bool Manager::winner() {
if (player1.isAlive()){
    return true;
}
else
    return false;
}

void Manager::getWinnerName() {
if(winner()){
    cout<<"Zwycieza :"<<player1.getName()<<endl;
}
if(!winner()){
    cout<<"Przegrales"<<endl;
}
}
