//
// Created by c on 5/27/22.
//

#ifndef PROJEKT_KONCOWY_PLAYER_H
#define PROJEKT_KONCOWY_PLAYER_H
#include "string"


using namespace std;

class Player {
    int level;
    int health;
    int AD;
    string name;
public:
    Player(const string &n, int hp, int lvl, int ADamage);
    int getLevel();
    int getHealth();

    int getAD();
    string getName();
    bool isAlive();
    void hit(int damage);
    void defend(int defend);

    virtual void wait(int add);
};


#endif //PROJEKT_KONCOWY_PLAYER_H
