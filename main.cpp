//
// Created by c on 5/27/22.
//
#include <iostream>
#include "Player.h"
#include "HPlayer.h"
#include "Manager.h"
#include <SFML/Graphics.hpp>
#include "sfmlManager.h"

using namespace std;
void prezentacja_HPlayer(){
    HPlayer gracz1("Michalek");
    cout<<"Postac zyje?: "<<gracz1.isAlive()<<endl;
    cout<<"Punkty zycia: "<<gracz1.getHealth()<<endl;
    cout<<"Nazwa: "<<gracz1.getName()<<endl;
    cout<<"Punkty ataku: "<<gracz1.getAD()<<endl;
    cout<<"Poziom gracza: "<<gracz1.getLevel()<<endl;
}

void intro(){
    cout<<"Michal Richter 98"<<endl;
}
int main(){
intro();
HPlayer gracz1("Bohater");
Enemy gracz2("Wrog");
//Manager gra(gracz1, gracz2);
    sfmlManager gra2(gracz1,gracz2);
    sf::RenderWindow win(sf::VideoMode(1920, 1080), "My Window");
    win.setFramerateLimit(60);
    sf::Texture bckgrnd;
    bckgrnd.loadFromFile("../graphics/arena.png");
    sf::Sprite s(bckgrnd);
   // s.setScale(2,2);
    while (win.isOpen()) {
        sf::Event event;
        while (win.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                win.close();
        }

        win.clear();
       win.draw(s);
       gra2.play(win);
        win.display();
    }

/*
 ŁADOWANIE TEKSTUR SFML W KONSTRUKTORZE!!!! NIE W METODZIE DRAW()
 */

//gra.fight();



    return 0;
}