//
// Created by gracz on 2022-06-18.
//

#include "sfmlManager.h"

sfmlManager::sfmlManager(HPlayer &p1, Enemy &p2) : gracz(p1),wrog(p2) {
state=TRWA;
HPlevel.loadFromFile("../graphics/add2.png");
ADlevel.loadFromFile("../graphics/add1.png");
miecze.loadFromFile("../graphics/swords.png");
    menu1.loadFromFile("../graphics/menu.png");
    bohater.loadFromFile("../graphics/hero.png");
    barbarian.loadFromFile("../graphics/enemy.png");
    miecz.loadFromFile("../graphics/sword.png" );
    tarcza.loadFromFile("../graphics/armor.png" );
    czcionka.loadFromFile("../graphics/HARNGTON.TTF");
    HPbohater.setFont(czcionka);
    HPEnemy.setFont(czcionka);
    ADBohater.setFont(czcionka);
    ADEnemy.setFont(czcionka);
    nazwa1.setFont(czcionka);
    nazwa2.setFont(czcionka);
    leveltxt.setFont(czcionka);
    ADleveltxt.setFont(czcionka);
    HPleveltxt.setFont(czcionka);
    atak.setFont(czcionka);
    czekanie.setFont(czcionka);
    obrona.setFont(czcionka);
}

void sfmlManager::play(sf::RenderWindow &win) {
    sf::Sprite bohaters{bohater},barbarians{barbarian},menus{menu1};
    int level=1;
    string HP1= to_string(gracz.getHealth());
    string HP2= to_string(wrog.getHealth());
    string ltxt= to_string(level);
    string AD1= to_string(gracz.getAD());
    string AD2= to_string(level*5);
    nazwa1.setFillColor(sf::Color::Black);
    nazwa1.setOutlineThickness(1);
    nazwa1.setOutlineColor(sf::Color::Green);
    nazwa2.setOutlineThickness(1);
    nazwa2.setOutlineColor(sf::Color::Green);
    nazwa2.setFillColor(sf::Color::Black);
    HPbohater.setString("Zycie: " + HP1);
    HPbohater.setFillColor(sf::Color::Red);
    HPbohater.setOutlineThickness(1);
    HPbohater.setOutlineColor(sf::Color::Green);
    HPbohater.setPosition(200,400);
    HPEnemy.setString("Zycie: "+ HP2);
    HPEnemy.setFillColor(sf::Color::Red);
    HPEnemy.setOutlineThickness(1);
    HPEnemy.setOutlineColor(sf::Color::Green);
    HPEnemy.setPosition(1700,390);
    leveltxt.setString("Poziom: "+ltxt);
    leveltxt.setFillColor(sf::Color::Black);
    leveltxt.setOutlineThickness(1);
    leveltxt.setOutlineColor(sf::Color::Green);
    leveltxt.setPosition(900,100);
    leveltxt.setCharacterSize(40);
    ADBohater.setString("AD: "+ AD1);
    ADBohater.setFillColor(sf::Color::Black);
    ADBohater.setOutlineThickness(1);
    ADBohater.setOutlineColor(sf::Color::Green);
    ADBohater.setPosition(200,450);
    ADEnemy.setString("AD: "+ AD2);
    ADEnemy.setFillColor(sf::Color::Black);
    ADEnemy.setOutlineThickness(1);
    ADEnemy.setOutlineColor(sf::Color::Green);
    ADEnemy.setPosition(1700,440);

   menus.setPosition(0,830);
    bohaters.setPosition(300,300);
    barbarians.setPosition(1350,330);
    nazwa1.setPosition(455,250);
    nazwa1.setString(gracz.getName());
    nazwa2.setPosition(1470,280);
    nazwa2.setString(wrog.getName());
    win.draw(bohaters);
    win.draw(barbarians);
    win.draw(menus);
    win.draw(nazwa1);
    win.draw(nazwa2);
    win.draw(HPbohater);
    win.draw(HPEnemy);
    win.draw(leveltxt);
    win.draw(ADEnemy);
    win.draw(ADBohater);
    menu(win);
    levelUp(win);
//    while (state == TRWA and level<6) {
//        int poziomHP;
//        int critId=0;
//        poziomHP=gracz.getHealth();
//        while (gracz.isAlive() and wrog.isAlive()) {
//            int dmg1 = gracz.getAD();
           int dmg2 = level*5;
//            if(event.type==sf::Event::MouseButtonPressed){
//                if(event.mouseButton.button==sf::Mouse::Left and 100<event.mouseButton.x<150 and 900<event.mouseButton.y<1000){
//                 wrog.hit(dmg1);
//                        if(critId==1){
//                            gracz.wait(-10);
//                            critId--;
//                        }
//                }
//            }
//        }
//    }
}

void sfmlManager::levelUp(sf::RenderWindow &win) {
    sf::Sprite addHP{HPlevel},addAD{ADlevel};
    addHP.setPosition(1500,900);
    addAD.setPosition(1300,900);
    ADleveltxt.setString("+5 do ataku");
    ADleveltxt.setCharacterSize(25);
    ADleveltxt.setPosition(1260,850);
    ADleveltxt.setOutlineThickness(1);
    ADleveltxt.setOutlineColor(sf::Color::Green);
    ADleveltxt.setFillColor(sf::Color::Black);
    HPleveltxt.setString("+5 do obrony");
    HPleveltxt.setCharacterSize(25);
    HPleveltxt.setPosition(1460,850);
    HPleveltxt.setOutlineThickness(1);
    HPleveltxt.setOutlineColor(sf::Color::Green);
    HPleveltxt.setFillColor(sf::Color::Black);

    win.draw(addAD);
    win.draw(addHP);
    win.draw(ADleveltxt);
    win.draw(HPleveltxt);
}

void sfmlManager::menu(sf::RenderWindow &win) {
    sf::Sprite mieczs{miecz},tarczas{tarcza},mieczes{miecze};
    mieczs.setPosition(100,900);
    mieczes.setPosition(300,900);
    tarczas.setPosition(600,900);
    atak.setCharacterSize(25);
    obrona.setCharacterSize(25);
    czekanie.setCharacterSize(20);
    atak.setString("Atakuj");
    obrona.setString("Bron sie (+5 do HP)");
    czekanie.setString("Przygotuj sie (+15 do ataku)");
    atak.setPosition(90,850);
    czekanie.setPosition(210,855);
    obrona.setPosition(550,850);
   atak.setOutlineThickness(1);
    atak.setOutlineColor(sf::Color::Green);
    atak.setFillColor(sf::Color::Black);
    obrona.setOutlineThickness(1);
    obrona.setOutlineColor(sf::Color::Green);
    obrona.setFillColor(sf::Color::Black);
    czekanie.setOutlineThickness(1);
    czekanie.setOutlineColor(sf::Color::Green);
    czekanie.setFillColor(sf::Color::Black);

    win.draw(tarczas);
    win.draw(mieczs);
    win.draw(mieczes);
    win.draw(atak);
    win.draw(obrona);
    win.draw(czekanie);
}

bool sfmlManager::winner() {
    return false;
}

void sfmlManager::getWinnerName() {

}
