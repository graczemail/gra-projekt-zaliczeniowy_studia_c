//
// Created by x240 on 29.05.2022.
//

#ifndef PROJEKT_KONCOWY_HPLAYER_H
#define PROJEKT_KONCOWY_HPLAYER_H
#include "Player.h"

using namespace std;

class HPlayer: public Player {

public:
    int level;
    int health;
    int AD;

   explicit HPlayer(const string &name);


    int getLevel();

};


#endif //PROJEKT_KONCOWY_HPLAYER_H
