//
// Created by gracz on 2022-06-05.
//

#ifndef PROJEKT_KONCOWY_MANAGER_H
#define PROJEKT_KONCOWY_MANAGER_H
#include "HPlayer.h"
#include "Enemy.h"
#include "Player.h"
#include "iostream"

using namespace std;
enum GameState{RUNNING, LOST,WIN};
class Manager {
 HPlayer &player1;
 Enemy &player2;
GameState state;

public:
    Manager(HPlayer &p1,Enemy &p2);
    void fight();
    void levelUp();
    void menu();
    bool winner();
    void getWinnerName();
};


#endif //PROJEKT_KONCOWY_MANAGER_H
